package com.example.wlf.helloworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText n01, n02;
    int n1,n2,res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        n01 = findViewById(R.id.text1);
        n02 = findViewById(R.id.text2);
    }

    public void somar(View v) {

        n1 = Integer.parseInt(n01.getText().toString());
        n2 = Integer.parseInt(n02.getText().toString());

        res = n1+n2;

        Toast.makeText(this, "Soma é: " + res, Toast.LENGTH_LONG).show();
    }

    public void subtrair(View v) {

        n1 = Integer.parseInt(n01.getText().toString());
        n2 = Integer.parseInt(n02.getText().toString());

        res = n1-n2;

        Toast.makeText(this, "Subtração é: " + res, Toast.LENGTH_LONG).show();
    }

    public void dividir(View v) {

        n1 = Integer.parseInt(n01.getText().toString());
        n2 = Integer.parseInt(n02.getText().toString());

        res = n1/n2;

        Toast.makeText(this, "Divisão é: " + res, Toast.LENGTH_LONG).show();
    }

    public void multiplicar(View v) {

        n1 = Integer.parseInt(n01.getText().toString());
        n2 = Integer.parseInt(n02.getText().toString());

        res = n1*n2;

        Toast.makeText(this, "Multiplicação é: " + res, Toast.LENGTH_LONG).show();
    }

}
